#!usr/bin/env/python3
# -*- coding : utf-8 -*-

from pygame import *

def drawTable(screen, estado, players, posList, tam, movementTable):

    scale = 20
    cell = tam[1]/scale
    grid = [[None]*scale for n in range(scale)]
    adj = []
    
    for player in players:
        if(player.selecionado):
            pos = player.positionGrid
            adj.append( (pos[0]+1, pos[1]) )
            adj.append( (pos[0], pos[1]+1) )
            adj.append( (pos[0]-1, pos[1]) )
            adj.append( (pos[0], pos[1]-1) )

    x,y = 10,10
    cx,cy = 0,0
    for row in grid:
        for col in row:
            for player in players:
                if ( player.pos(cx,cy) ):
                    if(player.selecionado):
                        rect = Surface((cell-1,cell-1), SRCALPHA, 32)
                        rect.fill((0,0,255,64))
                        screen.blit(rect, (x,y))
                    player.show(screen, (x,y), cell)
                if ( (cx,cy) in adj ):
                    rect = Surface((cell-1,cell-1), SRCALPHA, 32)
                    rect.fill((0,255,0,32))
                    screen.blit(rect, (x,y))
                    rect = rect.get_rect()
                    rect = rect.move((x,y))
                    movementTable.append( (rect, (cx,cy)) )
                    
                elif( not ( (cx,cy) in posList ) ):
                    rect = Surface((cell-1,cell-1), SRCALPHA, 32)
                    rect.fill((0,0,0,32))
                    screen.blit(rect, (x,y))
                    break
                    
            x = x + cell
            cx += 1
        cy += 1
        y = y + cell
        cx = 0
        x = 10
        
def posPlayers(players):
    
    cx,cy = 2,9
    for player in players:
        player.positionGrid = (cx,cy)
        cx += 1
        if(cx > 4):
            cy += 1
            cx = 2
            

def table(screen, estado, movementTable):

    players = estado.playersList
    tam = (700,700)
    
    draw.rect(screen, (0,0,0),(5, 5, tam[0]+10, tam[1]+10))
    
    tabuleiro = image.load("../Assets/Tabuleiro/Y-zone.png")
    tabuleiro = transform.scale(tabuleiro, tam)
    screen.blit(tabuleiro, (10,10))
    
    if estado.genPlayers:
        posPlayers(players)
        estado.genPlayers = False
        
    posList = []
    for player in estado.playersList:
        posList.append(player.positionGrid)
        
    drawTable(screen, estado, players, posList, tam, movementTable)
    

def loadScene(screen, estado):

    background = image.load("../Assets/Background.png")
    screen.blit(background, (0,0))
    
    movementTable = []
    
    table(screen, estado, movementTable)
   
    for evento in event.get():
        if evento.type == QUIT:
            exit()
        if evento.type == MOUSEBUTTONDOWN:
        
            for player in estado.playersList:
                if player.rect.collidepoint(evento.pos):
                
                    for oplayer in estado.playersList:
                        if(oplayer.selecionado):
                            oplayer.selecionado = False
                    player.selecionado = True
            
            
            for element in movementTable:
                if element[0].collidepoint(evento.pos):
                    for player in estado.playersList:
                        if player.selecionado:
                            player.positionGrid = (element[1][0], element[1][1])
                            
