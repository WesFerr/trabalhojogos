#!usr/bin/env/python3
# -*- coding : utf-8 -*-

from pygame import *
from pygame.locals import *
from sys import exit
from spritesGame import *
from menuScene import *
from gameScene import *
from player import *
from gameState import *
            
def loadingScreen(screen):
    loading = image.load("../Assets/Loading.png")
    loading = transform.scale(loading, (1280,720))
    screen.blit(loading, (0,0))

def main():

    init()
    
    clock = time.Clock()
    screen = display.set_mode((1280,720))
    screen.set_alpha(255)
    estado = gameState(screen, "inMenu")
    display.set_caption("Zombicide")
    
    while True:
               
        if estado.atualState == "inMenu":
            loadMenu(screen, estado)
        if estado.atualState == "inGame":
            loadScene(screen, estado)
        #if estado.atualState == "loading":
        #    loadingScreen(screen)
            
            
        display.update()
        clock.tick(30)
    

main()
