#!usr/bin/env/python3
# -*- coding : utf-8 -*-

from pygame import *
from pygame.locals import *

class spriteButton(sprite.Sprite):
    def __init__(self, screen, link, (x,y), (wid, hei)):
        sprite.Sprite.__init__(self)
        self.imgButton = image.load(link).convert_alpha()
        self.posButton = (x,y)
        self.imgButton = transform.scale(self.imgButton, (wid, hei))
        screen.blit(self.imgButton, self.posButton)
        
        
    def collide(self, pos):
        return self.imgButton.get_rect(topleft=(self.posButton)).collidepoint(pos)
        

class spritePlayer(sprite.Sprite):
    
    def __init__(self, screen, (x,y), imgPlayer):
        sprite.Sprite.__init__(self)
        self.imgPlayer = image.load(imgPlayer).convert_alpha()
        self.posPlayer = (x,y)
        
    def show(self, screen):
        screen.blit(self.imgPlayer, self.posPlayer)
        
        
    def collide(self, pos):
        return self.imgPlayer.get_rect(topleft=(self.posPlayer)).collidepoint(pos)
