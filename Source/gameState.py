#!usr/bin/env/python3
# -*- coding : utf-8 -*-

from player import *

class gameState(object):

    playersList = []
    genPlayers = True
    
    def __init__(self, screen, atualState):
        self.screen = screen
        self.atualState = atualState
        
    def addPlayer(self, nome, imgPlayer):
        self.playersList.append(player(nome, imgPlayer))
