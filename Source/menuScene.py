#!usr/bin/env/python3
# -*- coding : utf-8 -*-

from pygame import *
from pygame.locals import *
from spritesGame import *

def showMenuPlayers(screen):
    Amy =  spritePlayer(screen, (100,300), "../Assets/Personagens/Amy.png")
    Amy.show(screen)
    Doug = spritePlayer(screen, (190,300), "../Assets/Personagens/Doug.png")
    Doug.show(screen)
    Josh = spritePlayer(screen, (295,300), "../Assets/Personagens/Josh.png")
    Josh.show(screen)
    Ned  = spritePlayer(screen, (410,300), "../Assets/Personagens/Ned.png")
    Ned.show(screen)
    Phil = spritePlayer(screen, (510,300), "../Assets/Personagens/Phil.png")
    Phil.show(screen)
    Wanda= spritePlayer(screen, (610,300), "../Assets/Personagens/Wanda.png")
    Wanda.show(screen)
    
    return [Amy, Doug, Josh, Ned, Phil, Wanda]

def loadMenu(screen, estado):
    background = image.load("../Assets/Background.png")
    screen.blit(background, (0,0))
    
    Logo = spriteButton(screen, "../Assets/Logo.png", (800, 250), (350,200))
    
    characters = showMenuPlayers(screen)
    
    for evento in event.get():
        if evento.type == QUIT:
            exit()
        if evento.type == MOUSEBUTTONDOWN:
        
            if characters[0].collide(evento.pos):
                estado.addPlayer("Amy", "../Assets/Personagens/Amy.png")
                
            if characters[1].collide(evento.pos):
                estado.addPlayer("Doug", "../Assets/Personagens/Doug.png")
                
            if characters[2].collide(evento.pos):
                estado.addPlayer("Josh", "../Assets/Personagens/Josh.png")
                
            if characters[3].collide(evento.pos):
                estado.addPlayer("Ned", "../Assets/Personagens/Ned.png")
                
            if characters[4].collide(evento.pos):
                estado.addPlayer("Phil", "../Assets/Personagens/Phil.png")
                
            if characters[5].collide(evento.pos):
                estado.addPlayer("Wanda", "../Assets/Personagens/Wanda.png")
                
            if Logo.collide(evento.pos) and estado.playersList != []:
                estado.atualState = "inGame"
