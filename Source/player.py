#!usr/bin/env/python3
# -*- coding : utf-8 -*-

from pygame import *
from pygame.locals import *
from spritesGame import *

class player(object):
    
    equip = []
    exp = 0
    positionGrid = (None,None)
    selecionado = False
    
    def __init__(self, nome, imgPlayer = None):
            self.image = image.load(imgPlayer)
            self.nome = nome
        
    def addExp(self, exp):
        self.exp += exp
        
    def show(self, screen, (x,y), scale):
        rect = self.image.get_rect()
        w,h = rect[2],rect[3]
        proporcao = h/scale
        temp = transform.scale(self.image, (w/proporcao,scale))
        screen.blit(temp, (x,y))
        self.rect = temp.get_rect()
        self.rect = self.rect.move(x,y)
        
    def pos(self, x,y):
        if(self.positionGrid == (x,y)):
            return True
        else:
            return False
            
    def get_rect(self):
        return self.rect
